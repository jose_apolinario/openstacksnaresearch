# README #

This large document captures most text developed for Jose Teixeira et. al. efforts on studying the OpenStack project with SNA.

Branches include: ICIS 2016 - paper to submitted to ICIS 2016

Work from JISA and SMJ SI should be merged here as well. 

all.tex includes: 
Color schemes - what was submitted where 
Abstracts under the color schemes 
text under the color schemes 

objectives: 
* *Provide a picture of the evolution of the research project writing/publishing efforts 
* *Minimize text what was written but not published 
* *Keep track of the copyright of each paragraph (was it already published, submitted, where? or it's free to go somewhere?)